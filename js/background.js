var checkInterval = Number(180000);
var todayDiscountsUrl = "http://api.2for1.pro/api/salecount?saleval=50&period=day&request_source=3";
var mainSiteUrl = "http://2for1.pro";
var notificationID = "2for1Notification";
var todayDiscountsStorageKey = "2for1todayDiscounts";

console.log("2for1 background js");

localStorage.removeItem(todayDiscountsStorageKey);

setInterval(function () {
    $.get(todayDiscountsUrl, null, function (data) {
        var newTodayDiscounts = parseInt(data);
        var storedTodayDiscounts = localStorage[todayDiscountsStorageKey];
        var diff = Number(0);
        var notificationOptions = {
            type: "basic",
            title: "2FOR1",
            message: "WE HAVE NEW DEALS AT OVER 50% OFF FOR YOU!",
            iconUrl: "img/icon128.png",
            buttons: [{
                title: "Cool! Let me see new deals!"
            }, {
                title: "Cancel"
            }]
        };
        if (newTodayDiscounts > 0) {
            console.log("data from background js = " + newTodayDiscounts);
            if (storedTodayDiscounts == undefined) {
                localStorage[todayDiscountsStorageKey] = newTodayDiscounts;
                console.log("storedTodayDiscounts == undefined (data from local storage) = " + localStorage[todayDiscountsStorageKey]);
                chrome.browserAction.setBadgeText({ text: "NEW" });
            } else {
                storedTodayDiscounts = localStorage[todayDiscountsStorageKey];
                console.log("storedTodayDiscounts=" + storedTodayDiscounts);
                console.log("newTodayDiscounts=" + newTodayDiscounts);
                diff = Number(newTodayDiscounts) - Number(storedTodayDiscounts);
                console.log("diff=" + diff);
                if (diff > 0) {
                    localStorage[todayDiscountsStorageKey] = newTodayDiscounts;
                    chrome.browserAction.setBadgeText({ text: "NEW" });
                    chrome.notifications.clear(notificationID, function() {});
                    chrome.notifications.create(notificationID, notificationOptions, function() {} );
                    chrome.notifications.onButtonClicked.addListener(function(notifId, btnIdx) {
                        if (notifId === notificationID) {
                            if (btnIdx === 0) {
                                window.open(mainSiteUrl);
                                chrome.notifications.clear(notificationID, function() {});
                            } else if (btnIdx === 1) {
                                chrome.notifications.clear(notificationID, function() {});
                            }
                        }
                    });

                } else {
                    chrome.browserAction.setBadgeText({ text: "" });
                }
            }
        }
    }, "text");
}, checkInterval);
