var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-55670415-1']);
_gaq.push(['_trackPageview']);

(function () {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = 'https://ssl.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
})();

function onPageDetailsReceived(pageDetails) {
    $('#url').val(pageDetails.url);
}

$(document).ready(function () {
    var pageIndex = Number(0);
    var itemsPerPage = Number(3);
    var todayDiscountsUrl = "http://api.2for1.pro/api/salecount?saleval=50&period=day&request_source=3";
    var totalDiscountsUrl = "http://api.2for1.pro/api/totalcount?request_source=3";
    var stores = {
        1: "dsw.com",
        2: "6pm.com",
        3: "macys.com",
        4: "nordstrom.com",
        5: "overstock.com",
        6: "kohls.com",
        7: "hm.com",
        8: "asos.com",
        9: "saksfifthavenue.com",
        10: "bloomingdales.com",
        11: "yoox.com",
        12: "onlineshoes.com",
        13: "lacoste.com",
        14: "diesel.com",
        15: "lastcall.com",
        16: "urbanoutfitters.com",
        17: "nordstromrack.com"
    };

    $('#getMoreButton').click(function () {
        console.log("pageIndex=" + pageIndex);
        pageIndex = Number(pageIndex) + 1;
        console.log("pageIndex=" + pageIndex);
        getProducts();
    });

    function getTotalDiscounts() {
        $.getJSON(totalDiscountsUrl, function (data) {
            $("#total-discounts-value").text(Number(data));
        });
    }

    function getTodayDiscounts() {
        $.getJSON(todayDiscountsUrl, function (data) {
            $("#today-discounts-value").text(Number(data));
        });
    }

    function getProducts() {
        var $divProductList = $("#product-list");
        var title, picUrl, prices, itemBlock = "";
        $.getJSON("http://api.2for1.pro/api/list?disfrom=50&disto=100&Pz=" + itemsPerPage + "&Pi=" + pageIndex + "&request_source=3&sort=uid", function (json) {
            $.each(json, function (i, item) {
                if (parseInt(item.Type) <= 50) {
                    discountLabelClass = "label-warning";
                } else {
                    discountLabelClass = "label-danger";
                }
                picUrl = '<div class="col-xs-2 text-left"><a href="' + item.Url + '" target="_blank"><img class="product-image" src="' + item.Picurl + '"></a></div>';
                title = '<div class="col-xs-7 text-left product-name"><p><span class="product-brand">' + item.Brand + '  </span>' + item.Title +
                    '</p><p class="product-store">Go To Store: <a href="' + item.Url + '" target="_blank">' + stores[item.Source] + '</a></p></div>';
                prices = '<div class="col-xs-3 text-right product-prices"><p class="product-old-price">$' + parseFloat(item.Price2).toFixed(2) +
                    '</p><p class="product-new-price">$' + parseFloat(item.Price1).toFixed(2) + '</p>' +
                    '<p class="product-discount"><span class="label ' + discountLabelClass + '">-' + item.Type + '%</span></p>' + '</div>';
                itemBlock = itemBlock + '<div class="row m-t-large m-l-small m-r-small product-item">' + picUrl + title + prices + '</div>';
            });
            $divProductList.empty();
            $divProductList.append(itemBlock);
        });
    }

    getTotalDiscounts();
    getTodayDiscounts();
    getProducts();

    chrome.runtime.getBackgroundPage(function (eventPage) {
        // Call the getPageInfo function in the event page, passing in
        // our onPageDetailsReceived function as the callback. This injects
        // content.js into the current tab's HTML
        eventPage.getPageDetails(onPageDetailsReceived);
    });

    $("#urlGroup").hide();

    var userEmail = localStorage["2for1email"];
    if (userEmail != undefined) {
        $('#email').val(userEmail);
    }

    $('#discountForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            email: {
                validators: {
                    emailAddress: {
                        message: 'The value is not a valid email address'
                    },
                    notEmpty: {
                        message: 'Email is required'
                    }
                }
            }
        }
    });

    $("#submitButton").on("click", function () {
        var $form = $("#discountForm"),
            emailValue = $form.find("input[name='email']").val(),
            urlValue = $form.find("input[name='url']").val(),
            discountValue = $form.find("input[name='discount']:checked").val(),
            actionUrl = $form.attr("action");
        localStorage["2for1email"] = emailValue;
        _gaq.push(['_trackEvent', 'Clicks', 'Button', 'Submit']);
        var posting = $.post(actionUrl, { url: urlValue, email: emailValue, discount: discountValue });
        posting.done(function () {
            window.close();
        });
        posting.fail(function () {
            $("#postError").empty().append("You get error from the server.");
        });
    });

    $("input[name='discount']").on("click", function () {
        _gaq.push(['_trackEvent', 'Clicks', 'Radio', $(this).filter(':checked').val()]);
    });
});





